.PHONY: build shell


build:
	docker build -t registry.tompaton.com/tompaton/mpf .


shell:
	docker run -it --rm \
	 --device=/dev/ttyUSB0 \
	 -v `pwd`:/src \
	 registry.tompaton.com/tompaton/mpf \
	 bash
