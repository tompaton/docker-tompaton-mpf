FROM python:3.6

RUN mkdir /src /build
WORKDIR /src

RUN apt-get update && apt-get -y install screen
RUN pip install --no-cache-dir pytest mpfshell mpy_cross==1.11 esptool pytest-cov pylint

CMD ["mpfshell"]
